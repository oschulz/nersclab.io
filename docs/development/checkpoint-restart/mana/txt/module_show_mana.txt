cori$  module show mana 
-------------------------------------------------------------------
/global/common/software/nersc/cle7/extra_modulefiles/mana/2021-05-24:

module-whatis	 MANA (MPI-Agnostic Network-Agnostic) transparent checkpointing tool, 
                 implemented in DMTCP (Distributed MultiThreaded Checkpointing) 
                 transparently checkpoints MPI applications in user-space -- with 
                 no modifications to user code or to the O/S. 
setenv	         MANA_ROOT /usr/common/software/mana/2021-05-24/intel 
setenv	         MANA_PMI_LIBDIR /usr/common/software/mana/2021-05-24/intel/lib/pmi 
setenv	         DMTCP_DL_PLUGIN 0 
setenv	         PMI_NO_PREINITIALIZE 1 
prepend-path     LD_LIBRARY_PATH /usr/common/software/mana/2021-05-24/intel/lib/dmtcp 
prepend-path     PATH . 
prepend-path     PATH /usr/common/software/mana/2021-05-24/intel/bin 
prepend-path     MANPATH /usr/common/software/mana/2021-05-24/intel/share/man 
setenv	         mana_VERSION 2021-05-24 
setenv	         MANA_DIR /usr/common/software/mana/2021-05-24/intel 
setenv	         SITE_MODULE_NAMES mana 
prepend-path     mana_PKGCONFIG_LIBS mpidummy 
prepend-path     PE_PKGCONFIG_PRODUCTS mana 
prepend-path     PKG_CONFIG_PATH /usr/common/software/mana/2021-05-24/intel/lib/dmtcp/pkgconfig 
-------------------------------------------------------------------
