# Current known issues

## Perlmutter

!!! warning "Perlmutter is not a production resource"
    Perlmutter is not a production resource and usage is not charged
    against your allocation of time. While we will attempt to make the
    system available to users as much as possible, it is subject to
    unannounced and unexpected outages, reconfigurations, and periods
    of restricted access. Please visit the [timeline
    page](systems/perlmutter/timeline/index.md) for more information
    about changes we've made in our recent upgrades.

!!! warning "Temporary Reduced Capacity on Perlmutter"
    Perlmutter requires physical maintenance of the cooling system
    that will take up to up to mid-March 2022 to complete. Rather than
    shut down the entire machine, NERSC will perform the maintenance
    in a rolling fashion with the aim of keeping 500 or more nodes
    available to users. Occasionally, some jobs may see decreased GPU
    performance during this time. We will try to keep as much of the
    system available as possible, but please understand that
    Perlmutter is not yet a production resource with any uptime
    guarantees.

NERSC has automated monitoring that tracks failed nodes, so please
only open tickets for node failures if the node consistently has poor
performance relative to other nodes in the job or if the node
repeatedly causes your jobs to fail.

### New issues

- No new issues. 

### Ongoing issues

- MPI users may hit `segmentation fault` errors when trying
  to launch an MPI job with many ranks due to incorrect
  allocation of GPU memory. We provide [more information
  and a suggested workaround](systems/perlmutter/index.md#known-issues-with-cuda-aware-mpi).
- Some users may see messages like `-bash:
  /usr/common/usg/bin/nersc_host: No such file or directory` when you
  login. This means you have outdated dotfiles that need to be
  updated. To stop this message, you can either delete this line from
  your dot files or check if `NERSC_HOST` is set before overwriting
  it. Please see our [environment
  page](environment/index.md#home-directories-shells-and-dotfiles)
  for more details.
- [Known issues for Machine Learning applications](machinelearning/known_issues.md)
- `collabsu` is not available. Please create a
  [direct login](accounts/collaboration_accounts.md#direct-login) with
  [sshproxy](systems/perlmutter/index.md#connecting-to-perlmutter-with-sshproxy)
  to login into Perlmutter
  or switch to a collaboration account on Cori and then login to Perlmutter.

!!! caution "Be careful with NVIDIA Unified Memory to avoid crashing nodes"
    In your code, [NVIDIA Unified Memory](https://developer.nvidia.com/blog/unified-memory-cuda-beginners/)
    might
    look something like `cudaMallocManaged`. At the moment, we do not have
    the ability to control this kind of memory and keep it under a safe
    limit. Users who allocate a large
    pool of this kind of memory may end up crashing nodes if the UVM
    memory does not leave enough room for necessary system tools like
    our filesystem client.
    We expect a fix in early 2022. In the meantime, please keep the size
    of memory pools allocated via UVM relatively small. If you have
    questions about this, please contact us.

## Cori

The Burst Buffer on Cori has a number of known issues, documented at [Cori Burst Buffer](filesystems/cori-burst-buffer.md#known-issues).
