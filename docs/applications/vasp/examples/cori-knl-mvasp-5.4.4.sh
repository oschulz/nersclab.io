#!/bin/bash
#SBATCH -J test_mvasp
#SBATCH -N 512 
#SBATCH -C knl
#SBATCH -q debug
#SBATCH -o %x-%j.out
#SBATCH -t 30:00

module load mvasp/5.4.4-knl

#run 512 VASP jobs simultaneously each running vasp_std with 1 KNL node (64 processes)
sbcast --compress=lz4 `which mvasp_std` /tmp/mvasp_std
srun -n 32768 -c4 --cpu-bind=cores /tmp/mvasp_std


